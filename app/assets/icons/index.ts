export * from "./add_shopping_cart";
export * from "./logo";
export * from "./new";
export * from "./add";
export * from "./remove";
export * from "./delete";

export type IconProps = { color?: string };
