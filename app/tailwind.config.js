const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    spacing: {
      ...defaultTheme.spacing,
      355: "355px",
      513: "513px",
      208: "208px", // card container height
    },
    fontFamily: {
      sans: ["Inter"],
    },
  },
};
