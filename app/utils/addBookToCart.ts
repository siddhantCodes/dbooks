import { AddToCartMutation } from "../generated";

export const addBookToCart = async (bookId: number) => {
  const body = JSON.stringify({
    bookId,
  });

  const req = await fetch("/api/cart/add", { method: "post", body });

  return (req.json() as any) as {
    data: AddToCartMutation;
    message: string | null;
  };
};
