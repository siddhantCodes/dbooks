export const proxyGqlRequest = async (
  body: Record<string, any>,
  headers: Record<string, string>
) => {
  return fetch(process.env.NEXT_PUBLIC_GRAPHQL_ENDPOINT, {
    method: "POST",
    body: JSON.stringify(body),
    headers,
  }).then((r) => r.json());
};
