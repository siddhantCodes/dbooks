export const createStripeSession = async ({
  amount,
  bookName,
}: {
  amount: number;
  bookName: string;
}) => {
  return await fetch("/api/cart/session", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ amount, bookName }),
  }).then((r) => r.json());
};
