import React, { useState } from "react";

export const Gallery = ({ images }: { images: string[] }) => {
  const [active, setActive] = useState(0);

  return (
    <>
      <img className="w-full rounded-lg" src={images[active]} alt="" />
      <div className="grid grid-cols-4 gap-y-4 gap-x-4 lg:gap-x-0 justify-items-center items-center mt-6">
        {images
          .map((image, i) => (
            <img
              className="rounded-lg"
              style={{ width: "90px" }}
              src={image}
              alt=""
              onClick={() => setActive(i)}
            />
          ))}
      </div>
    </>
  );
};
