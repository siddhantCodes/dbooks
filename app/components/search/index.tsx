import { useRouter } from "next/router";
import React from "react";
import { useState, ChangeEvent } from "react";
import { SearchBooksQuery, useSearchBooksQuery } from "../../generated";
import { gqlClient } from "../../utils";

export const Search = (): JSX.Element => {
  const [query, setQuery] = useState("");
  const { data, error, isLoading } = useSearchBooksQuery(gqlClient, {
    term: query,
  });

  const search = (e: ChangeEvent<HTMLInputElement>) => setQuery(e.target.value);

  const renderResults = () => {
    if (!query.length) return null;
    else if (isLoading) {
      return <Loader error={error as Error} />;
    } else if (data && data.search_books.length) {
      return <Results results={data} />;
    } else {
      return <Loader error={{ message: "No Results Found" }} />;
    }
  };

  return (
    <div className="mb-8 mt-24 flex flex-col items-center relative">
      <h1 className="text-green-300 font-bold text-2xl sm:text-3xl">
        What do you want to read?
      </h1>

      <div className="w-full mt-4">
        <input
          onChange={search}
          className="focus:outline-white py-3 px-4 rounded w-full"
          type="text"
          placeholder="Search for your favourite books."
        />
        {renderResults()}
      </div>
    </div>
  );
};

function Loader({ error }: {error: Error | {message: string}}) {
  return (
    <SearchBox>
      <li className="text-center text-xl text-green-50">
        {error ? error.message : "Loading..."}
      </li>
    </SearchBox>
  );
}

const Results = React.memo(function ({
  results,
}: {
  results: SearchBooksQuery;
}) {
  const router = useRouter();

  function openBookDetails(id: number): void {
    router.push(`/books/${id}`);
  }

  return (
    <SearchBox>
      {results.search_books.map((r) => (
        <li
          key={r.id}
          onClick={() => openBookDetails(r.id)}
          className="text-xl mb-4 pb-2 border-b border-red-400 text-green-300 cursor-pointer group"
        >
          <span className="group-hover:underline">{r.name}</span>

          <span className="text-base block mt-2 text-green-50">
            {r.description}
          </span>
        </li>
      ))}
    </SearchBox>
  );
});

function SearchBox({ children }: { children: React.ReactNode }) {
  return (
    <div className="bg-gray-800 mt-2 h-96 w-full absolute rounded p-4">
      <ul className="h-full w-full overflow-auto">{children}</ul>
    </div>
  );
}
