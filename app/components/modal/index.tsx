import { useRef } from "react";
import { useOnClickOutside } from "../../hooks/useOnClickOutside";
import ctl from "@netlify/classnames-template-literals";

export const Modal = ({
  children,
  action,
  title,
  open,
  setOpen,
  onAction,
}: {
  children: React.ReactNode;
  action?: string;
  onAction?: (e: React.MouseEvent) => void;
  title: string;
  open: boolean;
  setOpen: React.Dispatch<boolean>;
}) => {
  const modalRef = useRef<HTMLDivElement>();

  useOnClickOutside(modalRef, () => {
    setOpen(false);
  });

  if (!open) return null;

  return (
    <div
      ref={modalRef as any} // Type incmopatibility, look into it!
      style={{ height: "calc(100vh - 32px)", width: "calc(100vw - 32px)" }}
      className={modalStyles}
    >
      <div className="mb-2 flex">
        <button
          onClick={() => setOpen(false)}
          className="border-none bg-transparent"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="24"
            viewBox="0 0 24 24"
            width="24"
          >
            <path d="M0 0h24v24H0V0z" fill="none" />
            <path
              fill="#fff"
              d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"
            />
          </svg>
        </button>
        <h2 className="mr-6 text-center w-full text-xl font-semibold text-green-50">
          {title}
        </h2>
        <div />
      </div>
      <hr className="border-red-400" />
      <div className="mt-2 flex-1 text-green-50">{children}</div>
      <div>
        {action && (
          <button onClick={onAction} className={actionButtonStyles}>
            {action}
          </button>
        )}
      </div>
    </div>
  );
};

const modalStyles = ctl(`
  p-4 
  rounded-lg 
  bg-gray-800 
  max-w-lg 
  fixed 
  top-4 
  right-4 
  flex 
  flex-col z-10
`);

const actionButtonStyles = ctl(`
  w-full
  bg-red-400
  rounded-sm
  text-white
  py-2
  px-4
  font-semibold
`);
