import * as React from "react";
import ctl from "@netlify/classnames-template-literals";

type buttonProps = {
  children?: React.ReactNode;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  className?: string;
  label: string;
};

export const Button = ({
  children,
  onClick,
  className,
  label,
}: buttonProps): JSX.Element => {
  const classes = `${btnClasses} ${className}`;

  return (
    <button aria-label={label} className={classes} onClick={onClick}>
      {children}
    </button>
  );
};

export const ComboButton = ({
  children,
  onClick,
  className,
  icon,
  label,
}: buttonProps & { icon: JSX.Element }): JSX.Element => {
  const classes = `${btnClasses} flex items-center ${className}`;

  return (
    <button aria-label={label} className={classes} onClick={onClick}>
      <span className="mr-1">{icon}</span>
      {children}
    </button>
  );
};

export const IconButton = ({
  onClick,
  className,
  icon,
  label,
}: buttonProps & { icon: JSX.Element }): JSX.Element => {
  const classes = `${btnClasses} py-0 px-0 ${className}`;

  return (
    <button aria-label={label} className={classes} onClick={onClick}>
      {icon}
    </button>
  );
};

export const btnClasses = ctl(`
  font-xl
  px-4
  py-2
  text-white
  font-bold
  bg-red-400
  rounded-sm
`);
