import * as React from "react";

export const Card = ({
  children,
  handler,
}: {
  children: React.ReactNode;
  handler: React.MouseEventHandler<HTMLDivElement>;
}) => {
  return (
    <div onClick={handler} className="w-355 h-513 cursor-pointer">
      {children}
    </div>
  );
};

Card.Image = ({ src }: { src: string }) => {
  return <img className="w-full h-2/4 rounded-t-lg rounded-b-none" src={src} />;
};

Card.Container = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="group h-208 rounded-b-lg p-4 bg-gray-800 flex flex-col justify-between">
      {children}
    </div>
  );
};

Card.Title = ({ value }: { value: string }) => {
  return (
    <h2
      aria-label={value.trim()}
      title={value.trim()}
      className="text-green-300 group-hover:underline font-extrabold text-2xl mb-4"
    >
      {value.trim().substr(0, 23)}
      {value.trim().length > 23 && "..."}
    </h2>
  );
};

Card.Description = ({ value, children }) => {
  return (
    <p
      title={value.trim()}
      aria-label={value.trim()}
      className="text-green-50 text-base leading-4 mb-6"
    >
      {children}
    </p>
  );
};

Card.Action = ({ children }) => {
  return <div className="flex justify-end">{children}</div>;
};
