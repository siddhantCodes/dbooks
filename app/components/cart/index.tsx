import React from "react";
import { useQuery } from "react-query";
import { Books, CartQuery, Carts_Books } from "../../generated";
import { IconButton, Modal } from "../../components";
import { AddIcon, DeleteIcon, RemoveIcon } from "../../assets/icons";
import { createStripeSession, getStripe } from "../../utils";

type CartBooks = Pick<Carts_Books, "multiplier"> & {
  book: Pick<Books, "name">;
};

export const Cart = ({
  open,
  setOpen,
}: {
  open: boolean;
  setOpen: React.Dispatch<boolean>;
}) => {
  // show the items in modal
  // show option to increment/decrement cart item
  // remove item from a cart
  // checkout with stripe
  // clear all items

  const { isLoading, data: response, error } = useQuery<{ data: CartQuery }>(
    ["cart"],
    async () => {
      return fetch("/api/cart").then((r) => r.json());
    }
  );

  const handleCheckout = async () => {
    const session = await createStripeSession({
      bookName: "Cart Test",
      amount: response!.data.carts[0].amount,
    });

    const stripe = await getStripe();
    const { error } = await stripe!.redirectToCheckout({
      sessionId: session.id,
    });

    if (error) {
      console.log("checkout failed");
    }
  };

  if (error) {
    console.log(error);
  }

  if (!isLoading && (!response || (response as any).error)) {
    console.log("response", response);
    console.log("query error", error);
    return <p>Error! Check the console!</p>;
  }

  if (open) {
    return (
      <Modal
        title="Cart"
        action="Checkout"
        onAction={handleCheckout}
        open={open}
        setOpen={setOpen}
      >
        {isLoading ? (
          "Loading..."
        ) : (
          <Content books={response!.data.carts[0].carts_books} />
        )}
      </Modal>
    );
  }

  return null;
};

// TODO: requires changes after the image upload functionality
function Content({ books }: { books: Array<CartBooks> }) {
  return (
    <ul>
      {books.map((item, i) => {
        return (
          <li
            className="py-6 px-4 flex justify-between border-b border-gray-600"
            key={i}
          >
            <h3 className="text-lg font-bold">{item.book.name}</h3>
            <div className="mr-4 flex">
              <div className="flex items-center">
                <IconButton
                  onClick={() => {}}
                  icon={<RemoveIcon />}
                  label="Decrement Item count by 1"
                />
                <span className="px-4">{item.multiplier}</span>
                <IconButton
                  onClick={() => {}}
                  icon={<AddIcon />}
                  label="Increment Item count by 1"
                />
              </div>

              <IconButton
                onClick={() => {}}
                icon={<DeleteIcon />}
                label="Remove Item from the Cart"
                className="ml-8"
              />
            </div>
          </li>
        );
      })}
    </ul>
  );
}
