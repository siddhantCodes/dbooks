import { useUser } from "@auth0/nextjs-auth0";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { useCallback } from "react";
import { QueryClient } from "react-query";
import { dehydrate } from "react-query/hydration";

import { AddToCartIcon } from "../assets/icons";
import { ComboButton, Card, Search } from "../components";
import { PageLayout } from "../layouts";
import { AllBooksDocument, useAllBooksQuery } from "../generated";
import { addBookToCart, gqlClient } from "../utils";

export default function Home(): JSX.Element {
  const { user, isLoading: loading } = useUser();
  const { data, isError, isLoading, error } = useAllBooksQuery(gqlClient, {
    limit: 9,
  });

  const router = useRouter();

  const addToCartHandler = useCallback(
    async (bookId: number) => {
      if (!user) {
        router.push("/api/auth/login");
      }
      await addBookToCart(bookId);
    },
    [user]
  );

  if (isLoading || loading) return <p>loading...</p>;

  if (isError) {
    console.error(error);
    return <p style={{color: "white"}}>Some error happened! Check console</p>;
  }

  return (
    <PageLayout>
      <Search />

      <hr className="border-red-400" />
      <h3 className="mb-4 mt-8 text-bold text-3xl text-green-50">
        Newly Added
      </h3>

      <div className="grid md:grid-cols-2 w-full xl:grid-cols-3 justify-items-center justify-center">
        {data?.books.map((book) => (
          <Card handler={() => router.push(`/books/${book.id}`)} key={book.id}>
            <Card.Image src="http://source.unsplash.com/random/355x288" />
            <Card.Container>
              <Card.Title value={book.name} />
              <Card.Description value={book.description}>
                {book.description.trim().substr(0, 120)}
                {book.description.trim().length > 120 && "..."}
              </Card.Description>
              <Card.Action>
                <div className="flex items-end">
                  <p className="mr-4 text-green-50">${book.price || 0}</p>
                  <ComboButton
                    onClick={(e) => {
                      e.stopPropagation();
                      addToCartHandler(book.id);
                    }}
                    icon={<AddToCartIcon />}
                    label="Add to Card"
                  >
                    Add to Card
                  </ComboButton>
                </div>
              </Card.Action>
            </Card.Container>
          </Card>
        ))}
      </div>
    </PageLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  const client = new QueryClient();
  const variables = { limit: 9 };

  client.prefetchQuery(["allBooks", variables], () =>
    fetch(process.env.NEXT_PUBLIC_GRAPHQL_ENDPOINT as string, {
      method: "POST",
      body: JSON.stringify({
        query: AllBooksDocument,
        variables,
      }),
    })
  );

  return {
    props: {
      dehydratedState: dehydrate(client),
    },
  };
};
