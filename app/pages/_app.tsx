import { QueryClient, QueryClientProvider } from "react-query";
import { Hydrate } from "react-query/hydration";
import { ReactQueryDevtools } from "react-query/devtools";
import * as React from "react";
import { UserProvider } from "@auth0/nextjs-auth0";
import { AppProps } from "next/app";

import "../styles/global.css";

function MyApp({ Component, pageProps }: AppProps) {
  const clientRef = React.useRef<QueryClient>();

  if (!clientRef.current) {
    clientRef.current = new QueryClient();
  }

  return (
    <UserProvider>
      <QueryClientProvider client={clientRef.current}>
        <Hydrate state={pageProps.dehydratedState}>
          <Component {...pageProps} />
        </Hydrate>

        <ReactQueryDevtools />
      </QueryClientProvider>
    </UserProvider>
  );
}

export default MyApp;
