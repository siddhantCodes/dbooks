import { useRouter } from "next/router";
import { PageLayout } from "../../layouts";
import { useBookQuery } from "../../generated";
import { addBookToCart, gqlClient } from "../../utils";
import { Gallery, ComboButton } from "../../components";
import { AddToCartIcon } from "../../assets/icons";
import { useCallback } from "react";
import { useUser } from "@auth0/nextjs-auth0";

export default function Home(): JSX.Element {
  const {
    query: { id },
    push: redirect,
  } = useRouter();
  const { user } = useUser();
  const { data, error, isLoading } = useBookQuery(gqlClient, { id });

  const addToCartHandler = useCallback(
    async (bookId: number) => {
      if (!user) {
        redirect("/api/auth/login");
      }
      await addBookToCart(bookId);
    },
    [user]
  );

  if (isLoading) {
    return <PageLayout>Loading...</PageLayout>;
  }

  if (error) {
    console.error(error);
  }

  if (!data || !data.books_by_pk) {
    redirect("/");
    return <></>;
  }

  const images = [
    "https://picsum.photos/477/466",
    "https://picsum.photos/89/89",
    "https://picsum.photos/91/91",
    "https://picsum.photos/90/90",
    "https://picsum.photos/90/90",
    "https://picsum.photos/90/90",
    "https://picsum.photos/90/90",
    "https://picsum.photos/90/90",
  ];

  return (
    <PageLayout>
      <div className="md:flex md:justify-between">
        <div className="md:flex-1 md:mr-8 lg:mr-16">
          <Gallery images={images} />
        </div>
        <div className="md:flex-1 md:ml-8 lg:ml-16 flex flex-col mt-8 md:mt-0">
          <div className="overflow-auto" style={{ maxHeight: "70vh" }}>
            <h2 className="text-4xl font-bold text-green-300">
              {data.books_by_pk.name}
            </h2>
            <p className="text-green-50 mt-4">
              {data.books_by_pk.description}
            </p>
          </div>
          <ComboButton
            onClick={() => addToCartHandler(data.books_by_pk?.id)}
            icon={<AddToCartIcon />}
            className="mt-8 h-12 w-5/6"
            label="Add to Cart"
          >
            Add to Cart
          </ComboButton>
        </div>
      </div>
    </PageLayout>
  );
}
