import { useUser } from "@auth0/nextjs-auth0";
import ctl from "@netlify/classnames-template-literals";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { PageLayout } from "../layouts";

export default function NewBook() {
  const { user , isLoading} = useUser();
  const router = useRouter();

  const inputStyles = ctl(`
    w-full 
    px-4 
    py-2 
    bg-transparent 
    text-green-50
    rounded-sm 
    border 
    border-gray-700
    focus:outline-none 
    focus:border-green-300 
    shadow-inner 
    placeholder-gray-500
  `);

  const labelStyles = ctl(`
    text-green-400
    mr-4
  `);

  useEffect(() => {
    if (!isLoading && !user) router.push("/");
  }, [user]);

  return (
    <PageLayout>
      <div className="flex flex-col md:flex-row justify-between ">
        <div className="md:flex-1 md:mr-8 lg:mr-16">
          <h1 className="text-green-50">TODO: image upload component</h1>
        </div>
        <div className="md:flex-1 flex flex-col mt-8 md:mt-0">
          <div className="flex flex-col">
            <label htmlFor="name" className={labelStyles}>
              Name
            </label>
            <input
              name="name"
              type="text"
              className={inputStyles}
              placeholder="Give your book a name!"
            />
          </div>
          <div className="flex flex-col">
            <label htmlFor="author" className={labelStyles}>
              Author
            </label>
            <input
              name="author"
              type="text"
              className={inputStyles}
              placeholder="Author Name"
            />
          </div>
          <div className="flex flex-col">
            <label htmlFor="description" className={labelStyles}>
              Decription
            </label>
            <textarea
              rows={7}
              name="description"
              className={inputStyles}
              placeholder="Write somemthing awesome about this book!"
            />
          </div>
        </div>
      </div>
    </PageLayout>
  );
}
