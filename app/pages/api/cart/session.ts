import { NextApiHandler } from "next";
import { StatusCodes } from "http-status-codes";
import Stripe from "stripe";
import { withApiAuthRequired } from "@auth0/nextjs-auth0";

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {} as any);

const handler: NextApiHandler = async (req, res) => {
  if (req.method === "POST") {
    const amount: number = req.body.amount;
    const bookName = req.body.bookName;

    try {
      if (!amount && amount <= 0) {
        throw new Error("Invalid amount.");
      }
      if (!bookName) {
        throw new Error("bookName is required.");
      }

      // Create Checkout Sessions from body params.
      // TODO: verify cart amount before creating the session
      // TODO: accept all cart item info
      const params: Stripe.Checkout.SessionCreateParams = {
        payment_method_types: ["card"],
        line_items: [
          {
            name: bookName,
            // convert Indian paise to rupees (1inr = 100paise)
            amount: Math.round(amount * 100),
            currency: "inr",
            quantity: 1,
          },
        ],
        // TODO: send an email to the user with download instructions
        // TODO: clear cart (probably via a webhook triggered by stripe)
        success_url: `${req.headers.origin}/?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${req.headers.origin}/?m="Payment Failed!"`,
      };

      const checkoutSession = await stripe.checkout.sessions.create(params);

      res.status(StatusCodes.OK).json(checkoutSession);
    } catch (err) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: err.message,
      });
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(StatusCodes.METHOD_NOT_ALLOWED).end("Method Not Allowed");
  }
};

export default withApiAuthRequired(handler);
