import { NextApiHandler } from "next";
import { getAccessToken, withApiAuthRequired } from "@auth0/nextjs-auth0";
import { StatusCodes } from "http-status-codes";
import { CartDocument } from "../../../generated";
import { proxyGqlRequest } from "../../../utils";

const handler: NextApiHandler = async (req, res): Promise<void> => {
  if (req.method === "GET") {
    const { accessToken } = await getAccessToken(req, res, {
      refresh: true,
    });

    const body = {
      query: CartDocument,
    };

    const result = await proxyGqlRequest(body, {
      Authorization: `Bearer ${accessToken}`,
    });

    res.status(StatusCodes.OK).json(result);
  } else {
    res
      .status(StatusCodes.METHOD_NOT_ALLOWED)
      .json({ message: "GET requests on this resource is not allowed" });
  }
};

export default withApiAuthRequired(handler);
