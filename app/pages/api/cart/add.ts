import { getAccessToken, withApiAuthRequired } from "@auth0/nextjs-auth0";
import { StatusCodes } from "http-status-codes";
import { NextApiHandler } from "next";
import { AddToCartDocument } from "../../../generated";
import { proxyGqlRequest } from "../../../utils";

const handler: NextApiHandler = async (req, res) => {
  if (req.method === "POST") {
    const { bookId } = JSON.parse(req.body);
    const { accessToken } = await getAccessToken(req, res);

    const body = {
      query: AddToCartDocument,
      variables: { item: { book_id: bookId, multiplier: 1 } },
    };

    const result = await proxyGqlRequest(body, {
      Authorization: `Bearer ${accessToken}`,
    });

    res.status(200).json(result);
  } else {
    res
      .status(StatusCodes.METHOD_NOT_ALLOWED)
      .json({ message: "GET requests on this resource is not allowed" });
  }
};

export default withApiAuthRequired(handler);
