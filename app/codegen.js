module.exports = {
  schema: [
    {
      "http://localhost:9191/v1/graphql": {
        headers: {
          "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET,
        },
      },
    },
  ],
  documents: ["**/*.graphql"],
  overwrite: true,
  generates: {
    "generated/index.ts": {
      plugins: [
        "typescript",
        "typescript-operations",
        "typescript-react-query",
      ],
      config: {
        fetcher: 'graphql-request',
      },
    },
  },
};
