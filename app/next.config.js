module.exports = {
  async redirects() {
    return [
      {
        source: '/books',
        destination: '/',
        permanent: true,
      },
    ]
  },
}
