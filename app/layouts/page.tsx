import { ReactNode } from "react";
import { NavBar } from "./navbar";

export const PageLayout = ({ children }: { children: ReactNode }) => {
  return (
    <>
      <NavBar />
      <main className="w-full max-w-screen-xl mt-4 mx-auto px-4 lg:px-16 xl:px-0">
        {children}
      </main>
    </>
  );
};
