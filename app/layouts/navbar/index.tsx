import Link from "next/link";
import { useUser } from "@auth0/nextjs-auth0";
import { Logo, AddToCartIcon, NewItemIcon } from "../../assets/icons";
import { Cart } from "../../components";
import { useState } from "react";

export const NavBar = () => {
  const [showCart, setShowCart] = useState(false);
  // error returned from useUser for a better UX
  const { user } = useUser();

  return (
    <>
      <nav className="w-full max-w-screen-xl mx-auto py-4 px-4 xl:px-0 flex justify-between">
        <Link href="/">
          <a>
            <Logo />
          </a>
        </Link>

        {user && (
          <div className="flex">
            <button
              onClick={() => setShowCart((v) => !v)}
              className="p-4 text-white text-2xl rounded-sm hover:bg-gray-800"
            >
              <AddToCartIcon color="#F87171" />
            </button>

            <Link href="/new">
              <a
                className="ml-2 p-4 text-white text-2xl rounded-sm hover:bg-gray-800"
                aria-label="Post Your Book!"
              >
                <NewItemIcon color="#F87171" />
              </a>
            </Link>
          </div>
        )}
      </nav>
      {showCart && <Cart open={showCart} setOpen={setShowCart} />}
    </>
  );
};
