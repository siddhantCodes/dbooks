import { MutableRefObject, useEffect } from "react";

/**
  This hook allows you to detect clicks outside of a specified element. 

  https://usehooks.com/useOnClickOutside/
*/
export function useOnClickOutside<T extends HTMLElement | undefined>(
  ref: MutableRefObject<T>,
  handler: (e: any) => void
) {
  useEffect(() => {
    const listener = (event: any) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);
    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [ref, handler]);
}
