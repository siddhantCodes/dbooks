terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.37"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

resource "aws_s3_bucket" "dbooks_assets" {
  bucket = "dbooks.siddhant.codes"
  acl    = "public-read"
}

resource "aws_iam_user" "dbooks" {
  name = "dbooks"
}

resource "aws_iam_policy" "s3_read" {
  name        = "s3_read"
  description = "Allow read access to resource s3"
  policy      = data.aws_iam_policy_document.s3_read.json
}

resource "aws_iam_user_policy_attachment" "s3_read" {
  user       = aws_iam_user.dbooks.name
  policy_arn = aws_iam_policy.s3_read.arn
}

data "aws_iam_policy_document" "s3_read" {
  statement {
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["arn:aws:s3:::*"]
    effect    = "Allow"
  }
  statement {
    actions   = ["s3:*"]
    resources = [aws_s3_bucket.dbooks_assets.arn]
    effect    = "Allow"
  }
}
