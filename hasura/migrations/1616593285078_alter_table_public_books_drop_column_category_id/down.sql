ALTER TABLE "public"."books" ADD COLUMN "category_id" int8;
ALTER TABLE "public"."books" ALTER COLUMN "category_id" DROP NOT NULL;
ALTER TABLE "public"."books" ADD CONSTRAINT books_category_id_fkey FOREIGN KEY (category_id) REFERENCES "public"."categories" (id) ON DELETE restrict ON UPDATE restrict;
