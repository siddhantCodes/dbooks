DROP TRIGGER IF EXISTS update_cart_price ON public.carts_books;
DROP TRIGGER IF EXISTS calculate_cart_price ON public.carts_books;

DROP FUNCTION IF EXISTS update_cart_price;
