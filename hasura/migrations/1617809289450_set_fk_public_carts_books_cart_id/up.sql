alter table "public"."carts_books"
           add constraint "carts_books_cart_id_fkey"
           foreign key ("cart_id")
           references "public"."carts"
           ("id") on update restrict on delete restrict;
