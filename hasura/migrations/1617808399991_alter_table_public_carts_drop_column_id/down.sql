ALTER TABLE "public"."carts" ADD COLUMN "id" int8;
ALTER TABLE "public"."carts" ALTER COLUMN "id" DROP NOT NULL;
ALTER TABLE "public"."carts" ALTER COLUMN "id" SET DEFAULT nextval('carts_id_seq'::regclass);
