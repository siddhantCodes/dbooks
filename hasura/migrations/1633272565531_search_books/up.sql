ALTER TABLE books ADD COLUMN fts tsvector 
  GENERATED ALWAYS AS (to_tsvector('english', name || ' ' || description)) 
  STORED;

CREATE INDEX books_search_idx ON books USING GIN (fts);
