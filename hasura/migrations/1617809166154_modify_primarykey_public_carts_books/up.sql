alter table "public"."carts_books" drop constraint "carts_books_pkey";
alter table "public"."carts_books"
    add constraint "carts_books_pkey" 
    primary key ( "book_id", "cart_id" );
