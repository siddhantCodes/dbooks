CREATE OR REPLACE FUNCTION public.update_cart_price ()
  RETURNS TRIGGER
  AS $$
DECLARE
  item record;
  new_amount numeric := 0;
BEGIN
  FOR item IN
  SELECT
    price,
    multiplier
  FROM
    carts_books
    JOIN books ON book_id = books.id
  WHERE
    cart_id = NEW.cart_id LOOP
      new_amount := new_amount + (item.price * item.multiplier);
    END LOOP;
  UPDATE
    carts
  SET
    amount = new_amount
  WHERE
    id = NEW.cart_id;
  RETURN new;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER update_cart_price
  AFTER UPDATE ON public.carts_books
  FOR EACH ROW
  EXECUTE PROCEDURE public.update_cart_price ();

COMMENT ON TRIGGER update_cart_price ON public.carts_books IS 'update the price of the related cart based on the updated cart item(s) in relation carts_books';

CREATE TRIGGER calculate_cart_price
  AFTER INSERT ON public.carts_books
  FOR EACH ROW
  EXECUTE PROCEDURE public.update_cart_price ();

COMMENT ON TRIGGER calculate_cart_price ON public.carts_books IS 'update the price of the related cart based on the updated cart item(s) in relation carts_books';

