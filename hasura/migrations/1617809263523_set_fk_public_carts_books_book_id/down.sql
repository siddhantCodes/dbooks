alter table "public"."carts_books" drop constraint "carts_books_book_id_fkey",
          add constraint "carts_books_book_id_fkey"
          foreign key ("book_id")
          references "public"."books"
          ("id")
          on update restrict
          on delete restrict;
