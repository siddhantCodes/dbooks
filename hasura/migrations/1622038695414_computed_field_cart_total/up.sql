CREATE OR REPLACE FUNCTION public.cart_total_computed_field(cart public.carts) 
  RETURNS numeric  
  LANGUAGE plpgsql
  STABLE
  AS $$
  DECLARE
    result numeric := 0;
  BEGIN
   SELECT SUM(price) FROM carts_books JOIN books ON book_id = id WHERE cart_id = cart.id INTO result;
   RETURN result;
  END;
$$;
