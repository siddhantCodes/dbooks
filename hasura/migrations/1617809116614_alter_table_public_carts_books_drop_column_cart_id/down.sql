ALTER TABLE "public"."carts_books" ADD COLUMN "cart_id" int8;
ALTER TABLE "public"."carts_books" ALTER COLUMN "cart_id" DROP NOT NULL;
