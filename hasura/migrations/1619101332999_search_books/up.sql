-- search term can be the book title or description
CREATE OR REPLACE FUNCTION search_books (term text)
  RETURNS SETOF public.search_result
  LANGUAGE plpgsql
  STABLE
  AS $$
BEGIN
  RETURN QUERY
  SELECT
    name,
    description,
    id
  FROM
    books
  WHERE
    fts @@ plainto_tsquery('english', term)
  LIMIT 10000; -- same number used in elasticsearch's default index.max_result_window
END;
$$
