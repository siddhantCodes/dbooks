ALTER TABLE "public"."carts" ADD COLUMN "user_id" int8;
ALTER TABLE "public"."carts" ALTER COLUMN "user_id" DROP NOT NULL;
