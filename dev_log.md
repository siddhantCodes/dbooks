# dbooks development logs

## Features I want to add in this project

1. Admin (domain/admin)
    - using hasura-graphql-engine for DB
    - nextjs for hasura client

2. Store (domain/)

### Admin

- Basic CRUD of books
- File uploads (epub, pdfs, and other digital reading formats)
- Authentication and Authorization
- Payment Handling

### Store

- Show the Friendly UI

## Things I will learn building this project

1. nextjs (haven't done any project with this before)
2. hasura (role based access to data)
3. react-query (the only sane hooks lib with no bloats)
4. auth0 (just a little bit)
5. tailwindcss (it will take a couple of projects to master this tech)
6. AWS (for hasura deployment and image upload functionality)

## Changes that I want to add to this project

1. Add tests (one more step towards perfection)
2. custom authentication and authorization

## Data access in hasura using auth0

After following the hasura auth0 guide, I am able to get JWTs from auth0 and send it to hasura. 
Hasura can verify these JWTs and is allowing access based on the user_id in these tokens.

Refreshing of these access_tokens is done by the auth0's nextjs lib. I just need to use the utils they provide to get 
the token.

We have two public roles in hasura:

1. guest (unauthenticated users)
2. user (authenticates users from auth0)


Guests can only read/select books.
Users can upload books, buy books, delete their books, etc.

The `HASURA_GRAPHQL_UNAUTHORIZED_ROLE` env variable can be used to set `guest` as the role to use for unauthenticated requests.
To disable the admin role, just set the `HASURA_GRAPHQL_ADMIN_SECRET`. After this all administrator requests will have to contain the 
`x-hasura-admin-secret` header with the value set in the environment variable.

### To get access_token in nextjs app:

1. In pages, one can use the data fetching functions provided by nextjs. 
  Inside them, call the `getAccessToken` async function provided by the `@auth0/nextjs-auth0` package.
  example:
  ```typescript

    export const getServerSideProps: GetServerSideProps = async (context) => {
      const {accessToken} = await getAccessToken(context.req, context.res)

      console.log(`accessToken: ${accessToken}`)

      return {
        props: {
          accessToken
        },
      };
    };
  ```

